
var start = null,
    result = null,
    correto = 0,
    errado = 0,
    timer = 0,

    element = document.getElementById("counter");

function letsPlay() {
    var qtd = parseInt(document.getElementById("value").value);
    var x = Math.trunc(Math.random() * Math.pow(10, qtd));
    var y = Math.trunc(Math.random() * Math.pow(10, qtd));

    var op = [];

    for (var i = 1; i <= 7; i++) {
        if (document.getElementById("op" + i).checked)
            op.push(i - 1);
    }

    var m = Math.trunc(Math.random() * 10) % parseInt(op.length);

    if (op[m] == 0) {
        result = x + y;
        m = " + ";
    } else if (op[m] == 1) {
        if(x >= y)
            result = x - y;
        else{
            x += y;
            y = x - y;
            x -= y;
            result = x - y;
        }
        m = " - ";
    } else if (op[m] == 2) {
        result = x * y;
        m = " * ";
    } else if (op[m] == 3) {
        result =  Math.trunc(x / ((y==0)?y=1:y) );
        m = " / ";
    } else if (op[m] == 4) {
        result = Math.pow(x, y);
        m = " ^ ";
    } else if (op[m] == 5) {
        result = x % y;
        m = " % ";
    } else if (op[m] == 6) {
        result = x * Math.trunc(Math.sqrt(y));
        m = " √ ";
    }

    document.getElementById("op").innerHTML = "<h1>" + x + m + y + " = </h1>";

    document.getElementById("resposta").focus();

}

function begin() {
    document.getElementById("score").innerHTML = "Acertos/Erros: " + correto + "/" + errado;
    document.getElementById("result").innerHTML = "<div class=warning>  Se Prepare </div>";
    document.getElementById("result").style.display = "inline-block";
    document.getElementsByClassName("btn")[0].style.display = "none";
    window.requestAnimationFrame(hold);
}

function myFunction(e) {
    var key = e.keyCode;
    alert(key);
    if ((key == 13 || key == 9) && (document.getElementById("resposta").value != "")) {
        alert(key);
        if (parseInt(document.getElementById("resposta").value) == result)
            correto++;
        else
            errado++;

        document.getElementById("score").innerHTML = "Acertos/Erros: " + correto + "/" + errado;
        document.getElementById("resposta").value = "";

        document.getElementById('resposta').focus();
        document.getElementById('resposta').select();
        letsPlay();
    }
}

function hold(timestamp) {
    if (!start) start = timestamp;
    var progress = timestamp - start;

    element.innerHTML = 3 - Math.trunc(progress / 1000);

    if (progress < 3500) {
        window.requestAnimationFrame(hold);
    }
    else {
        start = null;
        timer = parseInt(document.getElementById("timer").value);
        document.getElementById("result").innerHTML = "<input type=number onkeypress=myFunction(event) id=resposta>"
        window.requestAnimationFrame(step);

        letsPlay();
    }
}

function step(timestamp) {
    if (!start) start = timestamp;
    var progress = timestamp - start;

    element.innerHTML = Math.trunc(progress / 1000);

    if (progress <= (timer*1000)+500) {
        window.requestAnimationFrame(step);
    }
    else {
        document.getElementById("score").innerHTML = "Resultado: " + correto + " Acertos/ " + errado + " Erros";
        document.getElementById("op").innerHTML = "<h1> Acerto: " + Math.round((correto/(correto+errado)*100)) + "% </h1>";
        start = null;
        result = null;
        correto = 0;
        errado = 0;
        timer = 0;
        document.getElementById("result").style.display = "none";
        document.getElementsByClassName("btn")[0].style.display = "inline-block";
    }
}