var w = window.innerWidth - (window.innerWidth % 40),
    h = window.innerHeight - (window.innerHeight % 40),
    lastKey = 39;

document.getElementById('win').style.width = w;
document.getElementById('win').style.height = h;

document.getElementById('a').style.left = '200px';
document.getElementById('a').style.top = '40px';

for (var i = 'b'.charCodeAt(0); i<='e'.charCodeAt(0); i++){
    document.getElementById(String.fromCharCode(i)).style.left =  parseInt(document.getElementById(String.fromCharCode(i-1)).style.left) - 40 + 'px';
    document.getElementById(String.fromCharCode(i)).style.top =  document.getElementById(String.fromCharCode(i-1)).style.top;
}


function att() {
    for (var i = 'e'.charCodeAt(0); i >= 'b'.charCodeAt(0); i--) {
        document.getElementById(String.fromCharCode(i)).style.left = document.getElementById(String.fromCharCode(i - 1)).style.left;
        document.getElementById(String.fromCharCode(i)).style.top = document.getElementById(String.fromCharCode(i - 1)).style.top;
    }
}

document.onkeydown = function (e) {
    switch (e.keyCode) {
    case 37:
        if (lastKey == 39) break;
        att(); lastKey = 37;
        var x = parseInt(document.getElementById('a').style.left) - 40;
        document.getElementById('a').style.left = ((x) < 0 )? w-40 + "px" : x + "px";
        break;
    case 38:
        if (lastKey == 40) break;
        att(); lastKey = 38;
        var y = parseInt(document.getElementById('a').style.top) - 40;
        document.getElementById('a').style.top = ((y) < 0 )? h-40 + "px" : y + "px";
        break;
    case 39:
        if (lastKey == 37) break;
        att(); lastKey = 39;
        var x = parseInt(document.getElementById('a').style.left) + 40;
        document.getElementById('a').style.left = ((x) >= w )? 0 + "px" : x + "px";
        break;
    case 40:
        if (lastKey == 38) break;
        att(); lastKey = 40;
        var y = parseInt(document.getElementById('a').style.top) + 40;
        document.getElementById('a').style.top = ((y) >= h )? 0 + "px" : y + "px";
        break;
    }
}
